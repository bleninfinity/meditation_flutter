import 'package:hive/hive.dart';
import 'package:hive/hive.dart';

part 'database.g.dart';

@HiveType(typeId: 0)
class users{
  @HiveField(0)
 String name;
  @HiveField(1)
 String password;
  @HiveField(2)
 String email;
  @HiveField(3)
 int age;
  @HiveField(4)
 DateTime createDate;

}